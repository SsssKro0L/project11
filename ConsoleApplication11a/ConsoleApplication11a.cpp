﻿// PR11.cpp : Defines the entry point for the console application.
 //

#include "stdafx.h" 
#include <iostream> 
#include "openssl/conf.h"
#include "openssl/evp.h" 
#include "openssl/err.h" 
#include "openssl/aes.h" 
#include <fstream> 

 //Библиотеки opensll.f подключаются не явно динамически , компановка .lib на этапе сборки , второе - старт .dll на этапе запуска 
 // это значит , что сами функции в .dll 
 //А ссылки на них в .lib , а заголовки в папке include .h 


	using namespace std;
int main()
{

	//Работа с криптфункциями opensll 
	//1.Создание объекта с настройками 
	//2.Собсвенное шифрование 
	//3.Финализация и вывод зашифрованных данных 

	unsigned char *plaintext = (unsigned char *)" Some text Some text";// исходный текст 
	int plaintext_len = strlen((char *)plaintext); // длина текста 
	unsigned char *key = (unsigned char *)" 0123456789&quot"; // пароль (ключ) 
	unsigned char *iv = (unsigned char *)" 0123456789012345&quot";// инициализирующий вектор, рандомайзер 
	unsigned char cryptedtext[256]; // зашифрованный результат 
	unsigned char decryptedtext[256]; // расшифрованный результат 

									  //Структура в с++ почти как класс , различия минимальны 

									  /*1. Создаётся указатель на несуществующую структуру (сама структура ещё не заполнена)
									  структура - тип данных в с++, как класс, различия минимальны*/

	EVP_CIPHER_CTX *ctx; //structure 

						 /*2.Для указателя создаётся объект , на который он будет указан*/
	ctx = EVP_CIPHER_CTX_new(); //создание структуры с настройками метода 

								/*3.Структура evp_cipher_ctx заполняется настройками*/
	EVP_EncryptInit_ex(ctx, //ссылка на объект.структуру , куда заносятся параметры 
		EVP_aes_256_cbc(), //ссылка на шифр ядро aes 256 (функцию алгоритма) 
		NULL,
		key,//ключ \пароль 
		iv);// рандомайзер 

			//4. сам процесс шифрования - функция enecryptupdaseseeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee 
	int len;
	EVP_EncryptUpdate(ctx, // объект с настройками 
		cryptedtext, // входной параметр: ссылка, куда помещать зашифрованные данные 
		&len, // выходной параметр: длина полученного шифра 
		plaintext, // входной параметр: что шифровать 
		plaintext_len); // входной параметр : длина входных данных 
	int cryptedtext_len = len;



	// 5. Финализация процесса шифрования 
	// необходима, если последний блок заполнен данными не полностью 
	EVP_EncryptFinal_ex(ctx, cryptedtext + len, &len);
	cryptedtext_len += len;

	// 6. Удаление структуры 
	EVP_CIPHER_CTX_free(ctx);
	for (int i = 0; i < cryptedtext_len; i++)
	{
		cout << hex << cryptedtext[i];
		if ((i + 1) % 32 == 0) cout << endl;


	}
	cout << endl;

	// РАСШИФРОВКА 

	// 1. 
	ctx = EVP_CIPHER_CTX_new(); // создание структуры с настройками метода 

								// 2. 
	EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv); // инициализация методом AES, ключом и вектором 

															   // 3. 
	EVP_DecryptUpdate(ctx, decryptedtext, &len, cryptedtext, cryptedtext_len); // СОБСТВЕННО, ШИФРОВАНИЕ 

																			   // 4. 
	int dectypted_len = len;
	EVP_DecryptFinal_ex(ctx, decryptedtext + len, &len);

	// 5. 
	dectypted_len += len;
	EVP_CIPHER_CTX_free(ctx);
	decryptedtext[dectypted_len] = '\0';
	cout << decryptedtext << endl;


	//// файл для зашифрованных данных 
	//f_enctypted.open("f_enctypted.txt",
	//	std::fstream::out | std::fstream::trunc);

	//char buffer[256] = { 0 };
	//char out_buf[256] = { 0 };

	//ctx = EVP_CIPHER_CTX_new();
	//EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv);
	//len = 0;
	//f0.read(buffer, 256);
	//while (f0.gcount() > 0) // цикл, пока из файла что-то считывается (пока размер считанной порции > 0) 
	//{
	//	// шифрование порции 
	//	EVP_EncryptUpdate(ctx, // объект с настройками 
	//		(unsigned char *)out_buf, // входной параметр: ссылка, куда помещать зашифрованные данные 
	//		&len, // выходной параметр: длина полученного шифра 
	//		(unsigned char *)buffer, // входной параметр: что шифровать 
	//		f0.gcount()); // входной параметр : длина входных данных 

	//					  // вывод зашифрованной порции в файл 
	//	f_enctypted.write(out_buf, len);

	//	// считывание следующей порции 
	//	f0.read(buffer, 256);
	//}
	//EVP_EncryptFinal_ex(ctx, (unsigned char *)out_buf, &len);
	//f_enctypted.write(out_buf, len);
	//f_enctypted.close();
	//f0.close();


	//Шифрование файлов 
	//Создаются 2 файловых потока , временные буферы чтения записи , буфер для зашифрованных данных 
	//Цикл while работающий пока в файле остались не прочитанные данные в цикле : считыванние очередной порции из файла , шифрование порции 
	//запись шифра в другой файл 

	fstream f0, f_encrypted, f_decrypted;
	f0.open("f0.txt", std::fstream::in);// йфайл с исходными данными 

										//файл для зашифрованных данных 
	f_encrypted.open("f_encrypted.txt",
		std::fstream::out | std::fstream::trunc);

	char buffer[256] = { 0 };
	char out_buff[256] = { 0 };

	ctx = EVP_CIPHER_CTX_new();
	EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv);
	len = 0;
	f0.read(buffer, 256);
	while (f0.gcount() > 0)
	{
		EVP_EncryptUpdate(ctx,
			(unsigned char *)out_buff,
			&len,
			(unsigned char *)buffer,
			f0.gcount());


		f_encrypted.write(out_buff, len);


		f0.read(buffer, 256);
	}
	EVP_EncryptFinal_ex(ctx, (unsigned char
		*)out_buff, &len);
	f_encrypted.write(out_buff, len);
	f_encrypted.close();


	getchar();
	return 0;

}
